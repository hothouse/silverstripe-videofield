# README #

This module provides easy Youtube and Vimeo implementation in the Silverstripe admin area. It pulls a preview image and additional meta data about the video.

Requires Silverstripe 3.1

### What is this repository for? ###

* New video field with admin preview image & metadata
* Silverstripe 3.1

### How to use? ###

* creates new database field of type "Video" (to be used in private static $db array) like this:

        private static $db = array(
	      'Video' => 'Video'
        );

* creates new form field to be used in getCMSFields() function like this

        new VideoField('Video', 'Video');

* for Youtube you'll need to provide an API key in your config.yml like this:

        GoogleYouTube:
          api_key: 'YOUR_KEY'

### Who do I talk to? ###

* thomas@hothouse.co.nz
* Or just contact Hothouse @ www.hothouse.co.nz