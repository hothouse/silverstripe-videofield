<?php
namespace Hothouse\Videofield;

use SilverStripe\ORM\DB;
use SilverStripe\ORM\FieldType\DBComposite;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\View\ArrayData;

class Video extends DBComposite {

    private static $composite_db = array(
        "Url" => "Varchar",
        "Cache" => "Text"
	);

	function setUrl($val, $markChanged=true) {
		$this->setField('Url', $val);

		$Cache = self::get_video_data_to_cache($this->getUrl());
		$this->setCache($Cache);

		if($markChanged) $this->isChanged = true;
	}

	function setCache($val, $markChanged=true) {
		$this->setField('Cache', $val);
		if($markChanged) $this->isChanged = true;
	}

	function getUrl() {
		return $this->getField('Url');
	}

	function getCache() {
		return $this->getField('Cache');
	}

	public function VideoData() {
		if(is_null($this->CachedVideoData)) {
			$this->CachedVideoData = @unserialize($this->getCache());
		}
		return $this->CachedVideoData;
	}

	public function Embed($width = 450, $height = 366, $autoplay = 0, $params = '') {
		$Code = '';
		$VideoData = $this->VideoData();
		if($VideoData) {
			$params = $params ? '&'.$params : '';
			$params = str_replace('&amp;', '&', $params);
			$QueryString = sprintf('autoplay=%d%s', $autoplay, $params);
			$QueryString = htmlspecialchars($QueryString);
			$Code = sprintf(
				'<iframe%s%s%s src="%s?%s" allowfullscreen></iframe>',
				$width ? sprintf(' width="%d"', $width) : '',
				$height ? sprintf(' height="%d"', $height) : '',
				!$width && !$height ? ' class="embed-responsive-item"' : '',
				isset($VideoData->EmbedUrl) && $VideoData->EmbedUrl ? $VideoData->EmbedUrl : $VideoData->VideoUrl,
				$QueryString
			);
		}

		return $Code;
	}

	public function forTemplate($autoplay = 0, $params = '') {
		$Code = '';
		$VideoData = $this->VideoData();
		if($VideoData) {
			$params = $params ? '&'.$params : '';
			$params = str_replace('&amp;', '&', $params);
			$QueryString = sprintf('autoplay=%d%s', $autoplay, $params);
			$QueryString = htmlspecialchars($QueryString);
			$Code = sprintf(
				'<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="%s?%s" allowfullscreen></iframe>
				</div>',
				trim(isset($VideoData->EmbedUrl) && $VideoData->EmbedUrl ? $VideoData->EmbedUrl : $VideoData->VideoUrl),
				$QueryString
			);
		}

		return $Code;
	}

	public function scaffoldFormField($title = null, $params = null) {
		$field = new VideoField($this->name, $title);
		return $field;
	}

	public static function get_video_data_to_cache($sVideoUrl) {
		// get data
		$aVideoData = new ArrayData(array());

		if (strpos($sVideoUrl, 'vimeo')) {
			$aParts = explode('/', $sVideoUrl);
			$iVideoId = 0;
			foreach ($aParts as $sPart) {
				if(is_numeric($sPart) && strlen($sPart) > 5) {
					$iVideoId = $sPart;
					break;
				}
			}

			if($iVideoId) {
				// get info
				$sVideoInfo = @file('http://vimeo.com/api/v2/video/'.$iVideoId.'.json');

				// it might be hidden from the public
				$Embed = false;
				if($sVideoInfo === false) {
					$sVideoInfo = @file('http://vimeo.com/api/oembed.json?url=http%3A//vimeo.com/'.$iVideoId.'');
					$Embed = true;
				}

				// cant get data
				if($sVideoInfo === false) {
					return false;
				}

				// decode it
				$aData = json_decode($sVideoInfo['0'], true);

				if(!$Embed) {
					$aData = reset($aData);
				}

				$aVideoData = new ArrayData(array(
					'Type' => 'vimeo',
					'ID' => $iVideoId,
					'EmbedUrl' => '//player.vimeo.com/video/'.$iVideoId,
					'VideoUrl' => '//vimeo.com/'.$iVideoId,
					'ThumbnailUrl' => self::fix_url(isset($aData['thumbnail_large']) ? $aData['thumbnail_large'] : (isset($aData['thumbnail_url']) ? $aData['thumbnail_url'] : '')),
					'ThumbnailUrlMedium' => self::fix_url(isset($aData['thumbnail_medium']) ? $aData['thumbnail_medium'] : (isset($aData['thumbnail_url']) ? $aData['thumbnail_url'] : '')),
					'Description' => $aData['description'],
					'Title' => $aData['title']
				));
			}
		} elseif (strpos($sVideoUrl, 'youtube')) {
			if(strpos($sVideoUrl, 'v=')) {
				$sVideoId = substr($sVideoUrl, strpos($sVideoUrl, 'v=') + 2);
				if(strpos($sVideoId, '&') !== false) {
					$sVideoId = substr($sVideoId, 0, strpos($sVideoId, '&'));
				}
			} else {
				$aParts = explode('/', $sVideoUrl);
				$sVideoId = $aParts[array_search('embed', $aParts) + 1];
			}

			if($sVideoId) {
				$VideoInfo = GoogleYouTube::video($sVideoId);
				if($VideoInfo) {
					$aVideoData = array(
						'Type' => 'youtube',
						'ID' => $sVideoId,
						'EmbedUrl' => '//www.youtube.com/embed/'.$sVideoId,
						'VideoUrl' => '//www.youtube.com/watch?v='.$sVideoId,
						'ThumbnailUrl' => self::fix_url($VideoInfo['snippet']['thumbnails']['high']['url']),
						'ThumbnailUrlMedium' => self::fix_url($VideoInfo['snippet']['thumbnails']['medium']['url']),
						'Title' => $VideoInfo['snippet']['title'],
						'Description' => $VideoInfo['snippet']['description']
					);
					if(isset($VideoInfo['snippet']['thumbnails']['maxres'])) {
						$aVideoData['ThumbnailUrlHighRes'] = self::fix_url($VideoInfo['snippet']['thumbnails']['maxres']['url']);
					}
					$aVideoData = new ArrayData($aVideoData);
				}
			}
		} else {
			return false;
		}

		return serialize($aVideoData);
	}

	public static function fix_url($Url) {
		if(substr($Url, 0, 7) == 'http://') {
			$Url = substr($Url, 5);
		}
		return $Url;
	}
}
