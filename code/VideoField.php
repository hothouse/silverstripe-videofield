<?php
namespace Hothouse\Videofield;

use SilverStripe\Forms\FormField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObjectInterface;
use SilverStripe\ORM\FieldType\DBField;

class VideoField extends FormField {

	public function __construct($name, $title = null, $value = "") {
		$this->fieldUrl = TextField::create("{$name}Url", '')->setAttribute('placeholder', 'Video Url');
		$this->fieldCache = LiteralField::create("{$name}Cache", '');

		parent::__construct($name, $title, $value);
	}

	public function Field($properties = array()) {
		return "<div class=\"fieldgroup\">" .
			"<div class=\"fieldgroupField\">" . $this->fieldUrl->SmallFieldHolder() . "</div>" .
			"<div class=\"fieldgroupField\">" . $this->fieldCache->SmallFieldHolder() . "</div>" .
		"</div>";
	}

	public function setValue($val, $data = null) {
		$this->value = $val;

		if(is_array($data)) {
			if(isset($data['VideoUrl'])) {
				$this->fieldUrl->setValue($data['VideoUrl']);
			}
			if(isset($data['VideoCache'])) {
				$this->fieldCache->setValue($this->getLiteralFieldData(@unserialize($data['VideoCache'])));
			}
		} elseif($val instanceof Video) {
			$this->fieldUrl->setValue($val->getUrl());
			$this->fieldCache->setValue($this->getLiteralFieldData(@unserialize($val->getCache())));
		}

		return $this;
	}

	public function getLiteralFieldData($ArrayData) {
		if(!$ArrayData) {
			return '';
		}
		$Array = array(
			'<strong>Embed Url:</strong> '.$ArrayData->EmbedUrl,
			'<strong>Video Url:</strong> '.$ArrayData->VideoUrl,
			'<strong>Title:</strong> '.$ArrayData->Title,
			'<strong>Description:</strong> '.nl2br($ArrayData->Description),
			'<br /><img alt="" src="'.$ArrayData->ThumbnailUrl.'" />'
		);
		return implode('<br />', $Array);
	}

	public function saveInto(DataObjectInterface $dataObject) {
		$fieldName = $this->name;
		if($dataObject->hasMethod("set$fieldName")) {
			$dataObject->$fieldName = DBField::create_field('Video', array(
				"Url" => $this->fieldUrl->Value()
			));
		} else/*if(isset($dataObject->$fieldName))*/ {
			$dataObject->$fieldName->setUrl($this->fieldUrl->Value());
			//$dataObject->$fieldName->setCache($this->fieldCache->Value()); // automatically set by Url
		}
	}

}