<?php
namespace Hothouse\Videofield;

use SilverStripe\ORM;
use SilverStripe\Core\Config\Config;

class GoogleYouTube {

	/**
	 */
	const API_URL_YOUTUBE_VIDEO = 'https://www.googleapis.com/youtube/v3/videos';
	public static function video($id) {
		if(!($key = Config::inst()->get('GoogleYouTube', 'api_key'))) {
			throw new ORM\ValidationException(new ValidationResult(false, 'Please specify a Google YouTube API Key.'));
		}
		$service = new \GuzzleHttp\Client();
		try {
			$response = $service->request('GET', self::API_URL_YOUTUBE_VIDEO, ['query' => [
				'id' => $id,
				'key' => $key,
				'part' => 'snippet'
			]]);
		} catch (\Exception $E) {
			throw new ORM\ValidationException(new ValidationResult(false, $E->getMessage()));
		}

		if ($response->getStatusCode() != 200) {
			return false;
		}
		$data = json_decode($response->getBody(), true);
		if(isset($data['items']) && is_array($data['items']) && count($data['items'])) {
			$data = reset($data['items']);
			if(isset($data['snippet']) && isset($data['snippet']['thumbnails'])) {
				return $data;
			}
		}

		return false;
	}
}
